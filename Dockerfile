FROM registry.git.autistici.org/ai3/docker/apache2-base:bookworm

COPY conf /tmp/conf
COPY build.sh /tmp/build.sh
ADD https://github.com/hipages/php-fpm_exporter/releases/download/v2.2.0/php-fpm_exporter_2.2.0_linux_amd64 /usr/sbin/php-fpm-exporter

RUN /tmp/build.sh && rm -rf /tmp/build.sh /tmp/conf

