#!/bin/sh

target=http://localhost:8080/test.php

set -e
data=$(curl -sf "$target")
case "$data" in
ok)
    echo "Successfully executed test.php" >&2
    exit 0
    ;;
*)
    echo "Failed to execute test.php -- response:" >&2
    echo "$data"
    ;;
esac

exit 1

