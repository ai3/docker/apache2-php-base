#!/bin/sh
#
# Install script for apache2-users inside a Docker container.
#

# Packages that are only used to build the container. These will be
# removed once we're done.
BUILD_PACKAGES="rsync"

# Packages required to serve the website and run the services.
PACKAGES="
	systemd-standalone-tmpfiles

	php-cli
	php-curl
	php-fpm
	php-gd
	php-mbstring
	php-mysql
"

# The default bitnami/minideb image defines an 'install_packages'
# command which is just a convenient helper. Define our own in
# case we are using some other Debian image.
if [ "x$(which install_packages)" = "x" ]; then
    install_packages() {
        env DEBIAN_FRONTEND=noninteractive apt-get install -qy -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" --no-install-recommends "$@"
    }
fi

set -x
set -e

apt-get -q update
install_packages ${BUILD_PACKAGES} ${PACKAGES}

# Fix permissions on the php-fpm-exporter (installed via Dockerfile).
chmod 0755 /usr/sbin/php-fpm-exporter

# Set the PHP_FPM_EXPORTER_PORT in envvars, so that it is later picked up by
# the chaperone fpm-exporter service (APACHE_PORT is set at runtime).
echo "export PHP_FPM_EXPORTER_PORT=\`expr \$APACHE_PORT + 200\`" >> /etc/apache2/envvars

# Enable php configuration.
a2enmod proxy_fcgi setenvif
a2enconf -q php8.2-fpm

# Rsync our configuration, on top of /etc.
rsync -a /tmp/conf/ /etc/

# Remove packages used for installation.
apt-get remove -y --purge ${BUILD_PACKAGES}
apt-get autoremove -y
apt-get clean
rm -fr /var/lib/apt/lists/*
rm -fr /tmp/conf
